import axios from 'axios'

let catalogo = process.env.catalogo + "/v2/la-latino-car";
class CatalogosDirectos {
  marcas() {
    return axios({
      method: "get",
      url: catalogo + '/brands'
    })
  }
  modelos(marca) {
    return axios({
      method: "get",
      url: catalogo + `/years?brand=${marca}`
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: catalogo + `/models?brand=${marca}&year=${modelo}`
    }
    );
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: catalogo + `/variants?brand=${marca}&year=${modelo}&model=${submarca}`
    }
    );
  }
}

export default CatalogosDirectos;
