import axios from "axios"


//promoCore: "https://dev.core-persistance-service.com/v1/"




async function telefonoService (idDiffusionMedium) {
    return axios({
        method: "get",
        url: process.env.promoCore + '/v1/page/diffusion-medium/phone?idDiffusionMedium='+idDiffusionMedium,    
    })
}


export default telefonoService;