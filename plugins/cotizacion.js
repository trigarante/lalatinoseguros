import axios from "axios";

const cotizacionService = {};

cotizacionService.search = function (data, accessToken) {
  return axios({
    method: "post",
    url: process.env.promoCore + '/v2/latino/quotation',
    headers: { Authorization: `Bearer ${accessToken}` },
    data: JSON.parse(data)
  })
};
export default cotizacionService;
