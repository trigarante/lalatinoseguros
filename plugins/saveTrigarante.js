import axios from 'axios'

const urlSale = process.env.promoCore

class SaveService {
    saveHubspot(peticion) {
        return axios({
            method: "post",
            headers: { 'Content-Type': 'application/json' },
            url: process.env.hubspot ,
            data: JSON.parse(peticion)
        })
    }
    saveProspecto(peticion, accessToken) {
        return axios({
            method: "post",
            headers: { 'Authorization': 'Bearer ' + accessToken },
            url: urlSale + '/v3/cotizaciones/branding',
            data: JSON.parse(peticion)
        })
    }
    saveCotizacion(peticion, accessToken, cotizacionAli) {
        return axios({
            method: "put",
            headers: {'Authorization': 'Bearer '+accessToken},
            url: urlSale + `/v1/cotizaciones-ali/${cotizacionAli}`,
            data: JSON.parse(peticion)
        })
    }
    saveLeadEmision(peticion, accessToken) {
        return axios({
            method: "post",
            headers: { Authorization: `Bearer ${accessToken}` },
            url: urlSale + '/v3/issue/request_online',
            data: JSON.parse(peticion)
          })
    }
    saveLeadInteraccion(peticion, accessToken){
        return axios({
            method: "post",
            headers: { Authorization: `Bearer ${accessToken}` },
            url: urlSale + '/v3/interaction/request_online',
            data: JSON.parse(peticion)
          })
    }
    saveDataCliente(peticion, accessToken){
        return axios({
            method: "post",
            headers: { Authorization: `Bearer ${accessToken}` },
            url: urlSale + '/v2/json_quotation/customer_record',
            data: JSON.parse(peticion)
        })
    }
}

export default SaveService