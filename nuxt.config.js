module.exports = {
  target: "static",
  /*
   ** Headers of the page
   */
  head: {
    title: "lalatinoseguros.mx",
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "La Latino Seguros" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "favicon.ico" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    //analyze:true,
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          //loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include bootstrap css
  css: ["static/css/bootstrap.min.css", "static/css/styles.css"],
  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],
  modules: ["@nuxtjs/axios"],
  env: {
    tokenData: "mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g=", // TOKEN DATA
    catalogo: "https://dev.ws-latino.com", // CATALOGO
    coreBranding: "https://dev.core-brandingservice.com/", // CORE
    motorCobro: "https://p.lalatino-comprasegura.com/", // MOTOR COBRO
    sitio: "https://p.lalatinoseguros.mx/", // SITIO
    urlValidaciones: "https://core-blacklist-service.com/rest", // VALIDACIONES
    promoCore: "https://dev.core-persistance-service.com",
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    Environment: "DEVELOP",
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compressor: { threshold: 9 },
  },
};
