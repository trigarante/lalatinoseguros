import Vuex from 'vuex'
import getTokenService from '~/plugins/getToken'

const createStore = () => {
    const tokenData = process.env.tokenData
    return new Vuex.Store({
        state: {
			idEndPoint: '',
			status: '',
			message: '',
            ambientePruebas: false,
            cargandocotizacion: false,
            msj: false,
            msjEje: false,
            sin_token: '',
            tiempo_minimo: 1.30,
            config: {
                time: 5000,
                reload: true,
                habilitarBtnEmision: false,
                habilitarBtnInteraccion: false,
                btnEmisionDisabled:false,
                aseguradora: '',
                cotizacion: true,
                emision: true,
                interaccion:true,
                descuento: 0,
                telefonoAS: "88902498",
                grupoCallback: '',
                from: '',
                idPagina: 0,
                idMedioDifusion : 0,
                idMedioDifusionInteraccion : 0,
                idMedioDifusionEcommerce : 0,
                idSubRamo:'',
                id_cotizacion_a:'0',
                dominioCorreo:'',                alert: true,
                asgNombre: "HDI",
                ipCliente: "",
                accessToken: "",
                cotizacionAli: "",
                textoSMS:'Gracias por cotizar, en un momento uno de nuestros ejecutivos se pondrá en contacto contigo.',
                desc:'',
                msi:'',
                promoLabel:'',
                promoImg: '',
                extraMsg: '',
                promoSpecial:false
            },
            ejecutivo: {
                nombre: '',
                correo: '',
                id: 0
            },
            formData: {
                idHubspot:"",
                aseguradora: '',
                marca: '',
                modelo: '',
                descripcion: '',
                detalle: '',
                clave: '',
                cp: '',
                nombre: '',
                telefono: '',
                gclid_field: '',
                correo: '',
                edad: '',
                fechaNacimiento: '',
                genero: '',
                emailValid: '',
                telefonoValid: '',
                codigoPostalValid: '',
                urlOrigen: '',
                utmSource:'',
                utmMedium:'',
                utmCampaign:'',
                utmId:'',
            },
            solicitud: {},
            cotizacion: {},
            cotizacionesAmplia: []
        },
        actions: {
            getToken() {
                return new Promise((resolve, reject) => {
                  getTokenService.search(tokenData)
                    .then(resp => {
                        if(typeof resp.data == 'undefined'){this.state.config.accessToken = resp.accessToken}
                        else if(typeof resp.data.accessToken != 'undefined'){this.state.config.accessToken = resp.data.accessToken;}
                      localStorage.setItem("authToken", this.state.config.accessToken);
                      resolve(resp)
                    }, error => {
                      reject(error)
                    })
                })
              },
        },
        mutations: {
            validarTokenCore: function (state) {
                try {
                    if (process.browser) {
                        if (localStorage.getItem("authToken") === null || localStorage.getItem("authToken") === 'undefined') {
                            state.sin_token = true;
                            console.log('NO HAY TOKEN...')
                        } else {
                            console.log('VALIDANDO TOKEN...');
                            state.config.accessToken = localStorage.getItem("authToken");
                            var tokenSplit = state.config.accessToken.split('.');
                            var decodeBytes = atob(tokenSplit[1]);
                            var parsead = JSON.parse(decodeBytes);
                            var fechaUnix = parsead.exp
                            /*
                            * Fecha formateada de unix a fecha normal
                            * */
                            var expiracion = new Date(fechaUnix * 1000);
                            var hoy = new Date(); //fecha actual
                            /*
                            * Se obtiene el tiempo transcurrido en milisegundos
                            * */
                            var tiempoTranscurrido = expiracion - hoy;
                            /*
                            * Se obtienen las horas de diferencia a partir de la conversión de los
                            * milisegundos a horas.
                            * */
                            var horasDiferencia = Math.round(((tiempoTranscurrido / 3600000) + Number.EPSILON) * 100) / 100;

                            if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                                state.sin_token = 'expirado'
                            }
                        }
                    }
                } catch (error2) {
                    console.log(error2)
                }
            }
        }
    })
};
export default createStore
